# 농사 게임

모든걸 농사짓자!

## 설치

```console
$ git clone https://github.com/newkincode/simple_farming_game.git
$ cd simple_farming_game
$ pip install pygame

$ python main.py
```

## 빌드

```console
> ./build.bat
```
이후 lib폴더와 assets 폴더와 data폴더를 build폴더 안 main폴더에 넣어줍니다(복사 붙여넣기)

## 조작법

-   `방향키`: 이동
-   `E`: 낫. 흙을 농작지로 바꿉니다.
-   `S`: 삽. 농작지를 흙으로 되돌립니다.
-   `F`: 괭이. 수확에 사용합니다.
-   `SPACE`: 달리기
-   `Z`: 선택 해제
-   `D`: 선택 항목 사용
-   `G`: 아이템 정보
-   `W`: 물뿌리게
-   `V`: 비타민
-   `A`: 판매
-   `B`: 구매
-   `H`: 도움말
-   `K`: 저장
-   `L`: 불러오기
-   `M`: 블록 선택
-   `N`: 채팅
-   `숫자`: 아이템 선택  
맥북의 경우 영어로 선택된 상태에서 게임을 플래이 해주세요
## TODO

-   [ ] 얼마나 자랐는지 표시
-   [ ] 얼마후에 썩는지 표시
-   [x] 오프닝 추가
-   [ ] 아이템 선택시 플래이어가 들고있는 이미지로 변경
-   [ ] 스크롤로 아이템 바꾸기
-   [x] 아이템을 bar로 표시하기(but 아직 보완할 부분이 많음)
-   [ ] 클릭 인벤토리
-   [ ] 상자에 아이템 넣어서 판매
-   [x] 게임 내에서 설정을 바꿀수 있을수 있도록 변경
-   [ ] 도전과제 만들기
-   [ ] 상자 제작
-   [ ] 타이틀화면 만들기

## 기여자

-   [newkincode](https://github.com/newkincode) - 메인테이너
-   [LynHan1110](https://github.com/LynHan1110) - 예정
-   [ky0422](https://github.com/ky0422) - `README.md` 작성, 그 외 버그 수정
-   [kdh8219](https://github.com/kdh8219) - 코드 리팩터링, 첫번째 관심자
-   [copilot](https://github.com/features/copilot) - 코드 작성 도우미